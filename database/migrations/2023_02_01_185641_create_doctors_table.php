<?php

use App\Models\City;
use App\Models\PaymentMethod;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(City::class)->constrained()->onDelete('cascade');
            $table->foreignIdFor(PaymentMethod::class)->constrained()->onDelete('cascade');
            $table->string('full_name');
            $table->string('phone')->unique();
            $table->string('specialization');
            $table->string('image');
            $table->string('exp');
            $table->string('cvv');
            $table->boolean('status'); // if the doctor is available or not
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
};
