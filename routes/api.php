<?php

use App\Http\Controllers\Admin\adminController;
use App\Http\Controllers\Admin\authController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\cityController;
use App\Http\Controllers\Doctor\DoctorAppointmentController;
use App\Http\Controllers\Doctor\DoctorAuthController;
use App\Http\Controllers\Doctor\DoctorHomeController;
use App\Http\Controllers\Salesman\SalesmanAuthController;
use App\Http\Controllers\Salesman\SalesmanHomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get_cities' , [cityController::class , 'get_all_cities']);
Route::get('get_payment_methods' , [cityController::class , 'get_all_payment_methods']);
Route::get('admin/view-orders' , [adminController::class , 'get_orders']);
Route::post('admin/action/{id}' , [adminController::class , 'action']);
Route::post('admin/add-company' , [CompanyController::class , 'addCompany'])->middleware('auth:admin');


////////////////  # admin Routes # ///////////////////////
Route::controller(authController::class)->group(function () {
    //->middleware('auth:admin')
    Route::post('admin/login', 'login');



});
////////////////  # end of admin Routes # ///////////////////////

/// ////////////////  # doctor Routes # ///////////////////////
Route::controller(DoctorAuthController::class)->group(function () {
    //->middleware('auth:admin')
    Route::post('doctor/login', 'login');
    Route::post('doctor/send-order', 'send_order');
    Route::post('doctor-update-profile', 'update_profile')->middleware('auth:doctor');
    Route::get('doctor-profile', 'profile')->middleware('auth:doctor');

});
Route::controller(DoctorHomeController::class)->middleware('auth:doctor')->group(function () {
   Route::get('doctor/home', 'home');
    Route::get('doctor/my-interviews', 'my_interviews');
    Route::get('doctor/get-salesman', 'get_salesman_as_company');
      Route::get('doctor/get-salesman/{id}', 'get_salesman_as_company_byId');
    Route::get('doctor/get-products/{id}', 'get_salesman_products');
     Route::get('doctor/get-salesman-appointment' , 'get_salesman_as_company_withAppointment');
     Route::get('doctor/get-days' , 'get_days');

});
Route::controller(DoctorAppointmentController::class)->middleware('auth:doctor')->group(function () {
    Route::post('doctor/appointment-create', 'set_appointmet');
    Route::get('doctor/appointment-get', 'get_my_appintment');
    Route::post('doctor/appointment-status', 'isAvailable');
    Route::put('doctor/appointment-update/{id}', 'update');
    Route::post('doctor/num-visit', 'visit');
    Route::post('doctor/num-visit-update/{id}', 'update_visit');
    Route::get('doctor/get-num-visit', 'get_visit');
    



});
//////////////////  # end of doctor Routes # ///////////////////////
/// ////////////////  # Salesman Routes # ///////////////////////
Route::controller(SalesmanAuthController::class)->group(function () {
    //->middleware('auth:admin')
    Route::post('salesman-login', 'login');
    Route::post('salesman-register', 'register');
    Route::post('salesman-update-profile', 'update_profile');
    Route::get('salesman-profile', 'profile');


});
Route::controller(SalesmanHomeController::class)->middleware('auth:salesman')->group(function () {
    //->middleware('auth:admin')
    Route::get('salesman-doctors', 'doctor_listByCity');
    Route::get('salesman-search-doctors', 'search_for_doctor');
    Route::get('salesman-doctor/{id}', 'get_a_doctor');
    Route::get('salesman-doctor-appointment/{id}', 'get_doctor_appointment');
    Route::post('salesman-get-appointment', 'get_an_appointment');
    Route::get('salesman-get-my-appointment', 'get_my_appointment');



});
//////////////////  # end of Salesman Routes # ///////////////////////
