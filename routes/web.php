<?php

use App\Http\Controllers\Admin\authController;
use App\Http\Controllers\cityController;
use App\Http\Controllers\CP\doctorCPController;
use App\Http\Controllers\CP\salesManCPController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\adminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('admin/login' , [authController::class , 'login'])->name('admin_login');
Route::post('admin/add-new-admin' , [authController::class , 'add_admin'])->name('add_admin');
Route::get('admin/create-profile' , [authController::class , 'create_admin'])->name('create_admin');
Route::post('admin/update-profile/{id}' , [authController::class , 'update_profile_admin'])->name('update_profile_admin');
Route::get('admin/edit-profile/{id}' , [authController::class , 'edit_profile_admin'])->name('edit_profile_admin');
Route::get('admin/index' , [authController::class , 'admin_index'])->name('admin_index');



Route::get('/logout' , [authController::class , 'logout'])->name('logout');
Route::get('dashboard' , [DashboardController::class , 'dashboard'])->name('dashboard');
Route::get('doctor/list', [doctorCPController::class , 'index'])->name('doctor_index');
Route::get('doctor/edit/{id}', [doctorCPController::class , 'edit'])->name('doctor_edit');
Route::post('doctor/update/{id}', [doctorCPController::class , 'update'])->name('doctor_update');
Route::get('doctor/delete/{id}', [doctorCPController::class , 'destroy'])->name('doctor_delete');
///////////////////////////////////////////////////////////////////
Route::get('slesman/list', [salesManCPController::class , 'index'])->name('salesman_index');
Route::get('slesman/edit/{id}', [salesManCPController::class , 'edit'])->name('salesman_edit');
Route::post('slesman/update/{id}', [salesManCPController::class , 'update'])->name('salesman_update');
Route::get('slesman/delete/{id}', [salesManCPController::class , 'destroy'])->name('salesman_delete');

Route::get('get_cities' , [cityController::class , 'get_all_cities'])->name('city');
Route::get('get_citiesCP' , [cityController::class , 'get_all_citiesCP'])->name('cityCP');
Route::get('create_cities' , [cityController::class , 'create_city'])->name('create_city');
Route::post('store_cities' , [cityController::class , 'store_city'])->name('store_city');
Route::get('create_cities/{id}' , [cityController::class , 'edit_city'])->name('edit_city');
Route::post('store_cities/{id}' , [cityController::class , 'update_city'])->name('update_city');
Route::get('delete_cities/{id}' , [cityController::class , 'delete_city'])->name('delete_city');

Route::get('admin/action/{id}' , [adminController::class , 'action'])->name('reject');
Route::get('admin/action-accept/{id}' , [adminController::class , 'action_accept'])->name('accept');
Route::get('admin/view-orders' , [adminController::class , 'get_orders'])->name('orders'); 
Route::get('admin/view-rejected-orders' , [adminController::class , 'get_rejected_orders'])->name('rejected_orders');
Route::post('admin/charge/{id}' , [adminController::class , 'charge'])->name('charge');

//////////////////////////
Route::get('admin/methods' , [adminController::class , 'get_payments'])->name('get_payments');
Route::get('admin/create-methods' , [adminController::class , 'create_method'])->name('create_method');
Route::get('admin/edit-methods/{id}' , [adminController::class , 'edit_method'])->name('edit_method');
Route::post('admin/store-method' , [adminController::class , 'store_payment'])->name('store_payment');
Route::post('admin/update-method/{id}' , [adminController::class , 'update_payment'])->name('update_payment');
Route::get('admin/delete-method/{id}' , [adminController::class , 'delete_payment'])->name('delete_payment');


Route::get('charge-page/{id}' , [DashboardController::class , 'charge_page'])->name('charge_page');



//////////////////////////////////////
Route::post('admin/store-doctor' , [adminController::class , 'store_doctor'])->name('store_doctor');
Route::get('admin/create-doctor' , [adminController::class , 'create_doctor'])->name('create_doctor');

Route::post('admin/store-mandob' , [adminController::class , 'store_mandob'])->name('store_mandob');
Route::get('admin/create-mandob' , [adminController::class , 'create_mandob'])->name('create_mandob');






//Route::get('/redirect', [HomeController::class, 'redirect'])->name('redirect');
//Route::get('/send', [HomeController::class, 'send'])->name('send');

