<?php

namespace App\Rules;

use App\Models\Appointment_time;
use Illuminate\Contracts\Validation\Rule;

class TimeRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public  $day;
    public function __construct($day)
    {
        $this->day=$day;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
//        return false;
        $doctor=auth('doctor')->user()->times;
//        foreach ($doctor as $time){
//    $test=Appointment_time::where('doctor_id',auth('doctor')->user()->id);
    $dd= $doctor->where('day_id',$this->day)
        ->where('time',$value);

    if(count($dd) !=0)
        return  false;
    return  true;
        //            if($this->day==$time->day_id && $value == $time->time){
//                return false;
//            }else{
//                return true;
//            }
        }
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The time you chossed is already used in the same day';
    }
}
