<?php

namespace App\Rules;

use App\Models\Appointment_time;
use Illuminate\Contracts\Validation\Rule;

class TimeRuleUpdate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public  $day;
    public function __construct($day)
    {
        $this->day=$day;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $doctor=auth('doctor')->user()->times;
    
        $doctorTimes= $doctor->where('day_id',$this->day)->where('time',$value);
        foreach($doctorTimes as $times){
            if($times->time ==$value){
                return true;
            }
          
            
        }
    

    if(count($doctorTimes) !=0)return  false;
    return  true;

        }
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ' error from obada The time you chossed is already used in the same day';
    }
}
