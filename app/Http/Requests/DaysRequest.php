<?php

namespace App\Http\Requests;

use App\Rules\TimeRule;
use App\Rules\TimeRuleUpdate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DaysRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'day_id' => ['required', 'exists:days,id'],
                    'time' => ['required' , new TimeRule(request()->day_id)],
                    'duration' => ['required' ,'string'],
                    'price' => ['required', 'min:1'],
                    'kind_of_visite' => ['required', 'string'],
                    // 'max_visit'   => ['required'],
                ];
                break;
            case 'PUT':
                return [
            //        'day_id' => ['required', 'exists:days,id'],
                    'time' => ['required' , new TimeRuleUpdate(request()->day_id)],
                    'duration' => ['required', 'string'],
                    'price' => ['required', 'min:1'],
                    'kind_of_visite' => ['required', 'string'],
                ];
                break;
        }
    }
}
