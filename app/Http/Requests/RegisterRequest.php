<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
        return [
            'city_id'               =>['required' , 'exists:cities,id'],
            'payment_method_id'     =>['required' , 'exists:payment_methods,id'],
            'full_name'             =>['required' , 'string', 'min:3' , 'max:15'],
            'phone'                 =>['required' ,'min:10 , max:15' , 'unique:register_forms,phone'],
            'specialization'        =>['required' , 'string' , 'min:1' , 'max:30'],
            'image'                 =>['required' , 'image'],
            'exp'                   =>['nullable'],
            'cvv'                   =>['nullable'],
            'password'              =>['required' , 'string'],
            // 'max_visit'             =>['required' , 'numeric'],

        ];
                break;
            case 'PUT' :

                return [

                    //  'account_id'   => ['required' , 'exists:accounts,id'],
//                    'day'         => ['string' , 'max:10'],
//                    'date'      => ['date_format:Y-m-d'],
//                    'Payment_amount' => ['numeric'],
//                    'Note'    => ['string'],
//                    'status'  => ['boolean']
                ];
                break;
//            case 'DELETE':
//
//                return [
//                    'accounting_ids'=>[new AccountingRule()],
//                ];
//                break;
        }

    }
}
