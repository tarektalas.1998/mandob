<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesmanCPRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'                  => ['required' , 'string'],
            'city_id'               => ['required' , 'exists:cities,id'],
            'phone'                 =>['required' ,'min:10 , max:15' , 'unique:salesmen,phone,'. $this->route('id')],
            'office_name'           => ['required' , 'string' , 'max:50' , 'min:1'],
            'company'               => ['required' , 'string'],
            'specialization'        => ['required' , 'string'],
            'names'                 => ['required' , 'string'],
            'exp'                   =>['nullable'],
            'cvv'                   =>['nullable'],
           // 'password'              =>['required' , 'string'],
        ];
    }
}
