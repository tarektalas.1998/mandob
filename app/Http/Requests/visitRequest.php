<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class visitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'day_id'  => ['required' , 'exists:days,id', 'unique:doctor_days,id'],
            'max_visit' => ['required' , 'numeric'],
        ];
    }
}
