<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'city_id'               =>['required' , 'exists:cities,id'],
            'payment_method_id'     =>['required' , 'exists:payment_methods,id'],
            'full_name'             =>['required' , 'string', 'min:3' , 'max:25'],
            'phone'                 =>['required'],
            'specialization'        =>['required' , 'string' , 'min:1' , 'max:30'],
            'image'                 =>['nullable' , 'image'],
            'exp'                   =>['nullable'],
            'cvv'                   =>['nullable'],
            'password'              =>['required' , 'string'],
        ];
    }
}
