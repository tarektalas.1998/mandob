<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Doctor_day;
use App\Models\Wallet;
use App\Models\City;
use App\Models\Company;
use App\Models\Salesman;
use App\Http\Requests\SalesmanRegisterRequest;
use App\Http\Requests\StoreDoctorRequest;

use App\Models\Register_form;
use http\Env\Response;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class adminController extends Controller
{
    
      public function get_payments()
    {
        $data = PaymentMethod::all();
        return view('methods.list' , compact('data'));
    }

    public function create_method()
    {
        return view('methods.create');
    }

    public function edit_method($id)
    {
        $data = PaymentMethod::find($id);
        return view('methods.edit' , compact('data'));
    }

    public function store_payment(Request $request)
    {
        PaymentMethod::create([
            'name'  => $request->name,
        ]);

        return redirect()->route('get_payments')->with('message' , 'added');
    }

    public function update_payment(Request $request , $id)
    {
        $data = PaymentMethod::find($id);
        $data->update([
            'name' => $request->name,
        ]);
        return redirect()->route('get_payments')->with('message' , 'updated');
    }

    public function delete_payment($id)
    {

        $data = PaymentMethod::find($id);
        $data->delete();
        return redirect()->route('get_payments')->with('message' , 'deleted');
    }
    
    
     public function charge($id , Request $request)
    {
//        return $id;
        $man = Wallet::find($id);
//        return $man;
        $man->update([
            'balance' => $man->balance + $request->add,
        ]);
        return redirect()->route('salesman_index')->with('message' , 'added ' . $request->add . ' to wallet');
    }
    
    
    public function get_orders()
    {
        $data = Register_form::with('payment','city')->where('status' , 0)->orderBy('id' , 'desc')->get();

        return view('register_orders' , compact('data'));
    }
    
    public function get_rejected_orders()
    {
        $data = Register_form::with('payment','city')->where('status' , 2)->orderBy('id' , 'desc')->get();

        return view('rejected_orders' , compact('data'));
    }

    public function action(Request $request , $id)
    {
        $data = Register_form::find($id);

            $data->update([
                'status' => 2,   // 2 is rejected
            ]);

        return redirect()->back()->with('alert', 'this order is rejected successfully');



    }
    public function action_accept(Request $request , $id)
{
    $data = Register_form::find($id);

    $data->update([
        'status' => 1,
    ]);

    $input['city_id'] = $data->city_id;
    $input['payment_method_id'] = $data->payment_method_id;
    $input['full_name'] = $data->full_name;
    $input['phone'] = $data->phone;
    $input['specialization'] = $data->specialization;
    $input['password'] = $data->password;
    $input['image'] = $data->image;
    $input['exp'] = $data->exp;
    $input['cvv'] = $data->cvv;
    $input['status'] = 1;

    $doctor = Doctor::create($input);
   for($i = 1; $i <= 7; $i++){
        Doctor_day::create([
            'doctor_id' => $doctor->id,
            'day_id' => $i,
            'max_visit' => 5
            ]);
    }

    return redirect()->back()->with('message', 'this order is accepted successfully');


}

public function store_doctor(StoreDoctorRequest $request)
    {
        $data = $request->validated();
           $data['image'] = $request->image->store($request->full_name . 'image' , 'public');
        Doctor::create($data);
     
        return redirect()->back()->with('message' , 'doctor added');

    }

    public function create_doctor()
    {
        $cities = City::all();
        $pay = PaymentMethod::all();
        return view('Doctor.create' , compact('cities' , 'pay'));
    }

    public function store_mandob(SalesmanRegisterRequest $request)
    {
        $data = $request->validated();
        $input = Salesman::create($data);
        Wallet::create([
            'salesman_id' => $input->id,
        ]);


        $company['name'] = $request->company;
        $company['specialization'] = $request->specialization;

        Company::create($company);
        return redirect()->back()->with('message' , 'doctor added');

    }

    public function create_mandob()
    {
         $cities = City::all();
        return view('Salesman.create' , compact('cities'));
    }
    
}
