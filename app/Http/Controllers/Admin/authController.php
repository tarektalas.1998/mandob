<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Doctor;
use App\Models\Salesman;
use Illuminate\Support\Facades\Session;
use Cassandra\Exception\ValidationException;
use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use Illuminate\Support\Facades\Hash;

class authController extends Controller
{
    
      public function logout(Request $request) {
        //  Auth::logout();
        // $request->user()->currentAccessToken()->delete();
        // return response()->json('logged out');
        Session::flush();

        //Auth::logout();

        return view('welcome');
      }
      
    public function login(Request $request)
    {
        $admin = Admin::where('email', $request->email)->first();

//        return $admin;
        if (!$admin || !Hash::check($request->password, $admin->password)) {
            return redirect()->back()->with('message' ,'phone or password is incorrect');
        } else {
            $token = $admin->createToken($request->email);
            return redirect()->route('dashboard');
        }
    }
    
   
    public function admin_index()
    {
        $data = Admin::all();
        return view('admin.list' , compact('data'));
    }
    
    public function create_admin()
    {
        return view('admin.create');
    }
    
    public function edit_profile_admin($id)
    {
        $data  = Admin::find($id);
        return view('admin.edit' , compact('data'));
    }
    
    
    public function add_admin(AdminRequest $request)
    {
        $data = $request->validated();
        Admin::create($data);
        return redirect()->back()->with('message' , 'you add a new admin for the website');
    }
    
    public function update_profile_admin(Request $request , $id)
    {
        $data = Admin::find($id);
        $data->update([
            'full_name'  => $request->full_name ?? $data->full_name,
            'email'  => $request->email ?? $data->email,
            ]);
        if($request->password)
        {
             $data->update([
            'password'  => $request->password ?? $data->password,
           
            ]);
        }
        return redirect()->back()->with('messge' , 'updated');
    }
    
     public function delete_admin($id)
    {
        $data  = Admin::find($id);
        $data->delete();
        return redirect()->back()->with('message' , 'deleted');
    }
    
}
