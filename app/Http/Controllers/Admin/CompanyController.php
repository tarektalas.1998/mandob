<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function addCompany(CompanyRequest $request)
    {
        $data = $request->validated();

        Company::create($data);

        return response()->json('created', 201);
    }
}
