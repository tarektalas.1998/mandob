<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Http\Requests\DaysRequest;
use App\Http\Requests\visitRequest;
use App\Http\Resources\AppointmentResource;
use App\Http\Resources\visitResource;

use App\Models\Appointment_time;
use App\Models\Day;
use App\Models\Doctor_day;
use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorAppointmentController extends Controller
{
    public function set_appointmet(DaysRequest $request)
    {
        $num = Doctor_day::where('doctor_id' , auth('doctor')->user()->id)->count();
        if($num <= 7)
        {
             Appointment_time::create([
            'day_id'         =>$request->day_id,
            'time'           =>$request->time,
            'duration'       =>$request->duration,
            'price'          =>$request->price,
            'kind_of_visite' =>$request->kind_of_visite,
            'doctor_id'      => auth('doctor')->user()->id,
            // 'max_visit'      => $request->max_visit,
        ]);

        return response()->json('created' , 201);
        }
        else
        {
            return response()->json('you already have max visits for all your days' , 422);
        }
        
       
    }
    public function update(DaysRequest $request , $id)
    {
        $data = Appointment_time::find($id);
        $data->update([
           // 'day_id'            => $request->day_id ?? $data->day_id,
            'time'              => $request->time ?? $data->time,
            'duration'          => $request->duration ?? $data->duration,
            'price'             => $request->price ?? $data->price,
            'kind_of_visite'    => $request->kind_of_visite ?? $data->kind_of_visite,
        ]);
        return response()->json('updated' , 200);
    }
    public function get_my_appintment()
    {
        return AppointmentResource::collection(Appointment_time::with('day')->where('doctor_id' , auth('doctor')->user()->id)->get());

    }

    public function isAvailable(Request $request)
    {
        $data = Doctor::find(auth('doctor')->user()->id);
        if($request->isAvailable == 1)
        {
            $data->update([
                'isAvailable'   => 1
            ]);
            return response()->json('you are available for taking visits');
        }
        if($request->isAvailable == 0)
        {
            $data->update([
                'isAvailable'   => 0
            ]);
            return response()->json('you are unavailable for taking visits');
        }

    }
    public function visit(visitRequest $request)
    {
        $data['doctor_id'] = auth('doctor')->user()->id;
        $data['day_id'] = $request->day_id;
        $data['max_visit'] = $request->max_visit;
        
        Doctor_day::create($data);
        return response()->json('done' , 200);
    }
    
     public function update_visit(Request $request , $id)
    {
        $data = Doctor_day::find($id);
        $data->update([
            'max_visit' => $request->max_visit,
            ]);
       
        return response()->json('done' , 200);
    }
    
    public function get_visit()
    {
       
       return visitResource::collection(Doctor_day::with('day')->where('doctor_id' , auth('doctor')->user()->id)->get());
      
    }

}
