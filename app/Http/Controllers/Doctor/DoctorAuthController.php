<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Http\Requests\DoctorUpdateRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Doctor;
use App\Models\Register_form;
use Cassandra\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DoctorAuthController extends Controller
{
    public function send_order(RegisterRequest $request)
    {
        $data = $request->validated();
        $data['image'] = $request->image->store('RegisterForms_images' , 'public');
        Register_form::create($data);

        return response()->json($data , 200);
    }


    public function login(Request $request)
    {
        $doctor = Doctor::where('phone', $request->phone)->first();
        

        if(!$doctor)
        {
            return response()->json('Sorry, your request is not accepted from admin yet.' , 422);
        }
        if (!Hash::check($request->password, $doctor->password)) {
            return response()->json('phone or password is incorrect' , 422);
        }

        else
        {
            $token = $doctor->createToken($request->phone);
            return response()->json(['token' => $token->accessToken]);
        }
    }

    public function profile()
    {
        $data = Doctor::with('city' , 'payment')->where('id' , auth('doctor')->user()->id)->first();

        return response()->json($data);
    }

    public function update_profile(DoctorUpdateRequest $request )
    {


        $doctor = Doctor::where('id' , auth('doctor')->user()->id)->first();
        $doctor->update([
            'city_id'               => $request->city_id ?? $doctor->city_id,
            'payment_method_id'     => $request->payment_method_id ?? $doctor->payment_method_id,
            'full_name'             => $request->full_name ?? $doctor->full_name,
            'phone'                 => $request->phone ?? $doctor->phone,
            'specialization'        => $request->specialization ?? $doctor->specialization,
            'exp'                   => $request->exp ?? $doctor->exp,
             'cvv'                  => $request->cvv ?? $doctor->cvv,
            //  'max_visit'            => $request->max_visit ?? $doctor->max_visit,
        
        ]);
        if($request->password)
        {
            $doctor->update([
                'password' => $request->password,
            ]);
        }
        if($request->image)
        {
            $doctor->update([
                'image' => $request->image->store('RegisterForms_images' , 'public'),
            ]);
        }

        return response()->json('updated');
    }
}
