<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Http\Resources\DoctorHomeResource;
use App\Http\Resources\GetSalesmanAppointmentResource;
use App\Http\Resources\MyInterviewsResource;
use App\Models\Company;
use App\Models\Doctor;
use App\Models\Interview;
use App\Models\Day;
use App\Models\Product;
use App\Models\Salesman;
use Illuminate\Http\Request;

class DoctorHomeController extends Controller
{
   public function home()
    {
        return DoctorHomeResource::collection(Salesman::groupby('company')->distinct()->get());
    }

    public function get_salesman_as_company(Request $request)
    {
         $data = Salesman::with('city')->where('company' , $request->company)->get();

        return response()->json($data);
    }
      public function get_salesman_as_company_byId(Request $request , $id)
    {
        $data = Salesman::with('city')->where('id' , $id)->get();

        return response()->json($data);
    }
    
     public function get_salesman_as_company_withAppointment(Request $request)
    {
        $data = GetSalesmanAppointmentResource::collection(Salesman::with('city' , 'interview')->where('company' , $request->company)->get());

        return response()->json($data);
    }
    

    public function get_salesman_products($id)
    {
        $data = Product::where('company_id' , $id)->get();

        return response()->json($data);
    }

    public function my_interviews()
    {
//        $data = Interview::with('sadsad' , 'salesman')->where('doctor_id' , auth('doctor')->user()->id)->get();
//
//        return response()->json($data);
        return MyInterviewsResource::collection(Interview::with('sadsad' , 'salesman')->where('doctor_id' , auth('doctor')->user()->id)->get());

    }
    
     public function get_days()
    {
        $data = Day::all();

        return response()->json($data);
    }
    
}
