<?php

namespace App\Http\Controllers;

use Hawkiq\LaravelZaincash\Services\ZainCash;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function send()
{
    $zaincash = new ZainCash();
    //The total price of your order in Iraqi Dinar only like 1000 (if in dollar, multiply it by dollar-dinar exchange rate, like 1*1500=1500)
    //Please note that it MUST BE MORE THAN 1000 IQD
    $amount = 500;

    //Type of service you provide, like 'Books', 'ecommerce cart', 'Hosting services', ...
    $service_type="Shirt";

    //Order id, you can use it to help you in tagging transactions with your website IDs, if you have no order numbers in your website, leave it 1
    $order_id="20222009";

    $payload =  $zaincash->request($amount, $service_type, $order_id);
    return $payload;
}
public function redirect(Request $request)
{
    $token = $request->input('token');
    if (isset($token)) {
        $zaincash = new ZainCash();
        $result = $zaincash->parse($token);
        return $result;
        if ($result->status == 'success'){ // success ||  failed  || pending
            return 'Thanks for Buying';
            // We can do what ever you like , insert transaction into database, send email etc..
        }
    }

}
}
