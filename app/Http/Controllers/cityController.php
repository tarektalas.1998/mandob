<?php

namespace App\Http\Controllers;

use App\Http\Requests\cityRequest;
use App\Models\City;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class cityController extends Controller
{

    public function get_all_cities()
    {
        $data = City::all();
        return response()->json($data);
    }

    public function get_all_citiesCP()
    {
        $data = City::all();
        return view('city.list' , compact('data'));
    }

    public function get_all_payment_methods()
    {
        $data = PaymentMethod::all();
        return response()->json($data);
    }

    public function store_city(cityRequest $request)
    {
        $data = $request->validated();
            City::create($data);

        return redirect()->route('cityCP')->with('message' , 'added');
    }

    public function create_city()
    {
        return view('city.create');
    }

    public function edit_city($id)
    {
        $data = City::find($id);
        return view('city.edit' , compact('data'));
    }

    public function update_city(Request $request , $id)
    {
        $data = City::find($id);
        $data->update([
            'name'  => $request->name ?? $data->name,
        ]);

        return redirect()->route('cityCP')->with('message' , 'update');
    }
    
    public function delete_city($id)
    {
        $data = City::find($id);
        $data->delete();
        return redirect()->back()->with('message' , 'deleted');
    }


}
