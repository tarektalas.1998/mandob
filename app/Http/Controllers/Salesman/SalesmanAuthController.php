<?php

namespace App\Http\Controllers\Salesman;

use App\Http\Controllers\Controller;
use App\Http\Requests\SalesmanRegisterRequest;
use App\Http\Requests\SalesmanUpdateRequest;
use App\Http\Resources\GetSalesmanResource;
use App\Models\Salesman;
use App\Models\Wallet;
use Cassandra\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SalesmanAuthController extends Controller
{
    public function login(Request $request)
    {
         $salesman = Salesman::where('phone', $request->phone)->first();

       
        if (!$salesman || !Hash::check($request->password, $salesman->password)) {
            return response()->json('phone or password is incorrect' , 422);
        }

        else
        {
            $token = $salesman->createToken($request->phone);
            return response()->json(['token' => $token->accessToken]);
        }
    }

    public function register(SalesmanRegisterRequest $request)
    {

        $data = $request->validated();

        $input = Salesman::create($data);
        Wallet::create([
            'salesman_id' => $input->id,
        ]);
        
        $token = $input->createToken($request->phone);
        // $token = $input->createToken('authToken')->plainTextToken;
        // return response()->json(['token' => $token->accessToken]);

        return response()->json($token);

    }
     public function profile()
    {
      //  $data =  Salesman::with('city')->where('id' , auth('salesman')->user()->id)->first();
        //return $data;
         return GetSalesmanResource::collection(Salesman::with('city' , 'wallet')->where('id' , auth('salesman')->user()->id)->get());

    }

    public function update_profile(SalesmanUpdateRequest $request)
    {
        $data = Salesman::where('id' , auth('salesman')->user()->id)->first();

        $data->update([
            'name'               => $request->name    ?? $data->name,
            'city_id'            => $request->city_id ?? $data->city_id,
            'phone'              => $request->phone   ?? $data->phone,
            'office_name'        => $request->office_name ?? $data->office_name,
            'company'         => $request->company?? $data->company,
            'specialization'     => $request->specialization ?? $data->specialization,
            'names'              => $request->names ?? $data->names,
            'exp'                => $request->exp ?? $data->exp,
            'cvv'                => $request->cvv ?? $data->cvv,
        ]);
        if($request->password)
        {
            $data->update([
                'password' => $request->password,
            ]);
        }
        return response()->json('updated');
    }

}
