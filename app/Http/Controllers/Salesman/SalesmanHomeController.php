<?php

namespace App\Http\Controllers\Salesman;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetAppointmentRequest;
use App\Http\Resources\SalesmanDoctorAppointmentResource;
use App\Http\Resources\SalesmanDoctorsResource;
use App\Http\Resources\InterviewResource;
use App\Http\Resources\SalesmanDoctorsSearchResource;
use App\Models\Appointment_time;
use App\Models\Doctor;
use App\Models\Wallet;
use App\Models\Interview;
use Illuminate\Http\Request;

class SalesmanHomeController extends Controller
{
  public function doctor_listByCity(Request $request)
    {
        return SalesmanDoctorsResource::collection(Doctor::with('city' , 'payment' , 'times')->where('city_id' , $request->city_id)->get());
    }

    public function search_for_doctor()
    {
        return SalesmanDoctorsSearchResource::collection(Doctor::all());
    }
    public function get_a_doctor($id)
    {
        return SalesmanDoctorsResource::collection(Doctor::with('city' , 'payment')->where('id' , $id)->get());
    }

    public function get_doctor_appointment($id)
    {
        return SalesmanDoctorAppointmentResource::collection(Appointment_time::with('day')->where('doctor_id' , $id)->get());
    }

    public function get_an_appointment(GetAppointmentRequest $request)
    {
        $data = $request->validated();
        $man = auth('salesman')->user()->id;
        $wallet = Wallet::where('salesman_id' , $man)->first();
        $price = Appointment_time::where( 'id' , $request->appointment_time_id)->first()->value('price');
        if($wallet->balance >= $price)
        {
            $data['salesman_id'] = auth('salesman')->user()->id;
            $inter = Interview::create($data);
            $wallet->update([
                'balance' => $wallet->balance - $price,
            ]);
            return response()->json('appointment taken successfully');
        }
        else{
            return response()->json('You dont have enough money ');
        }
    }
     public function get_my_appointment()
    {
        $data = InterviewResource::collection(Interview::with('salesman' , 'doctor')->where('salesman_id' , auth('salesman')->user()->id)->get());
       // $data = Interview::with('sadsad' , 'doctor')->where('salesman_id' , auth('salesman')->user()->id)->get();
        return response()->json($data);

    }
}
