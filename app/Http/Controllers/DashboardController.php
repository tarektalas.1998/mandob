<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\Doctor;
use App\Models\Salesman;
use App\Models\Company;
use App\Models\Register_form;


class DashboardController extends Controller
{
    public function dashboard()
    {
        $doctors = Doctor::count();
        $salesman = Salesman::count();
        $company = Company::count();
          $forms = Register_form::count();
      
        return view('index' , compact('doctors' , 'salesman' , 'company' , 'forms'));
    }
    
     public function charge_page($id)
    {
        $data = Wallet::where('salesman_id' , $id)->first();

       // return $data;
        return view('Accounting' , compact('data'));
    }
}
