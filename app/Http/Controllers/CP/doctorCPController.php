<?php

namespace App\Http\Controllers\CP;

use App\Http\Controllers\Controller;
use App\Http\Requests\DoctorUpdateRequest;
use App\Models\City;
use App\Models\Doctor;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class doctorCPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::with('interview' ,'city' , 'payment')->paginate(10);
       // $doctors1 = Doctor::with('interview')->paginate(10);
        

        return view('Doctor.list' , compact('doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctor = Doctor::with('city' , 'payment')->where('id' , $id)->first();
        $cities = City::all();
        $payments = PaymentMethod::all();
        return view('Doctor.edit' , compact('doctor' , 'cities' , 'payments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DoctorUpdateRequest $request, $id)
    {

        $doctor = Doctor::find($id);

        $doctor->update([
            'city_id'               => $request->city_id ?? $doctor->city_id,
            'payment_method_id'     => $request->payment_method_id ?? $doctor->payment_method_id,
            'full_name'             => $request->full_name ?? $doctor->full_name,
            'phone'                 => $request->phone ?? $doctor->phone,
            'specialization'        => $request->specialization ?? $doctor->specialization,
            'exp'                   => $request->exp ?? $doctor->exp,
            'cvv'                   => $request->cvv ?? $doctor->cvv,
       
        ]);
        if($request->password)
        {
            $doctor->update([
                'password' => $request->password,
            ]);
        }
        if($request->image)
        {
            $doctor->update([
                'image' => $request->image->store('RegisterForms_images' , 'public'),
            ]);
        }
        return redirect()->route('doctor_index')->with('message' , 'updated succsesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor = Doctor::find($id);

        $doctor->delete();
        return redirect()->route('doctor_index')->with('message' , 'deleted succsesfully');

    }
}
