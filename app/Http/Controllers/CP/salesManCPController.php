<?php

namespace App\Http\Controllers\CP;

use App\Http\Controllers\Controller;
use App\Http\Requests\SalesmanCPRequest;
use App\Models\City;
use App\Models\Company;
use App\Models\PaymentMethod;
use App\Models\Salesman;
use Illuminate\Http\Request;

class salesManCPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salesmen = Salesman::with('city' )->paginate(10);

        return view('Salesman.list' , compact('salesmen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salesman = Salesman::with('city' )->where('id' , $id)->first();
        $cities = City::all();

        return view('Salesman.edit' , compact('salesman' , 'cities' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalesmanCPRequest $request, $id)
    {
        $doctor = Salesman::find($id);

        $doctor->update([
            'name'             => $request->name ?? $doctor->name,
            'city_id'          => $request->city_id ?? $doctor->city_id,
            'phone'            => $request->phone ?? $doctor->phone,
            'office_name'      => $request->office_name ?? $doctor->office_name,
            'company'         => $request->company?? $doctor->company,
            'specialization'   => $request->specialization ?? $doctor->specialization,
            'cvv'              => $request->cvv ?? $doctor->cvv,
            'exp'               => $request->exp ?? $doctor->exp,
            'names'            => $request->names ?? $doctor->names,
           
        ]);
        if($request->password)
        {
            $doctor->update([
                'password' => $request->password,
            ]);
        }


        return redirect()->route('salesman_index')->with('message' , 'updated succsesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Salesman::find($id);
        $data->delete();
        return redirect()->back()->with('message' , 'deleted');
    }
}
