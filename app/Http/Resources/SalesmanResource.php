<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesmanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'salesman_name'    => $this->name,
            'salesman_phone'    => $this->phone,
            'salesman_office_name'    => $this->office_name,
            'salesman_city'         => $this->city->name,
        ];
    }
}
