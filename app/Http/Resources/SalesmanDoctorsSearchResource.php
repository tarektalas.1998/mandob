<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesmanDoctorsSearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
           'id'            => $this->id,
            'doctor_name'   => $this->full_name,
            'phone'             => $this->phone,
            'specialization'    => $this->specialization,
            'image'             => $this->image,
            'exp'               => $this->exp,
            'cvv'               => $this->cvv,
            'isAvailable'       => $this->isAvailable,
            'city_name'         => $this->city->name,
            'payment_method'    => $this->payment->name,
            'appointment_info'     => AppointmentResource::collection($this->times),
        ];
    }
}
