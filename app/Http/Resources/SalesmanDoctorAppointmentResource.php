<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalesmanDoctorAppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'day_name'          => $this->day->name,
            'time'              => $this->time,
            'duration'          => $this->duration,
            'price'             => $this->price,
            'kind_of_visite'    => $this->kind_of_visite,
        ];
    }
}
