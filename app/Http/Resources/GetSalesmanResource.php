<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GetSalesmanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'city_name'  => $this->city->name,
             'wallet_balance'  => $this->wallet->balance,
            'company'   => $this->company,
            'name'   => $this->name,
            'phone'   => $this->phone,
            'office_name'   => $this->office_name,
            'specialization'   => $this->specialization,
            'exp'   => $this->exp,
            'cvv'   => $this->cvv,
            'names'   => $this->names,
        ];
    }
}
