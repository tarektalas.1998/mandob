<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InterviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'interview_id'  => $this->id,
            'doctor_name'  => $this->doctor->full_name,
            'phone'  => $this->doctor->phone,
            'specialization'  => $this->doctor->specialization,
            'cvv'  => $this->doctor->cvv,
            'payment_method'  => $this->doctor->payment->name,
            'time'     => $this->sadsad->time,
            'duration'     => $this->sadsad->duration,
            'price'     => $this->sadsad->price,
            'kind_of_visite'     => $this->sadsad->kind_of_visite,
            'day'     => $this->sadsad->day->name,

        ];
    }
}
