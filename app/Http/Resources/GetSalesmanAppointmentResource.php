<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GetSalesmanAppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
      //  return parent::toArray($request);
        return [
            'company_name'  => $this->company,
            'salesman_name'  => $this->name,
            'salesman_phone'  => $this->phone,
            'office_name'  => $this->office_name,
            'specialization'  => $this->specialization,
            'exp'  => $this->exp,
            'cvv'  => $this->cvv,
            'names'  => $this->names,
            'city_name'  => $this->city->name,
            'interview_info'  => $this->interview->sadsad->day->name,
            'interview_kind_of_visite'  => $this->interview->sadsad->kind_of_visite,
            'interview_price'  => $this->interview->sadsad->price,
            'interview_duration'  => $this->interview->sadsad->duration,
            'interview_time'  => $this->interview->sadsad->time,
            'doctor_name'  => $this->interview->doctor->full_name,


        ];
    }
}
