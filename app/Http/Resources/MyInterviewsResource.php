<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MyInterviewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'time'      => $this->sadsad->time,
            'duration'      => $this->sadsad->duration,
            'price'      => $this->sadsad->price,
            'kind_of_visite'      => $this->sadsad->kind_of_visite,
            'day'      => $this->sadsad->day->name,
            'salesman_name' => $this->salesman->name,
            'salesman_specialization' => $this->salesman->specialization,
            'salesman_names' => $this->salesman->names,
            'phone' => $this->salesman->phone,
            'office_name' => $this->salesman->office_name,
            'salesman_company' => $this->salesman->company,

        ];
    }
}
