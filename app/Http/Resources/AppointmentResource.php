<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'appointment_id' => $this->id,
            'time'          => $this->time,
            'duration'      => $this->duration,
            'price'         => $this->price,
            'kind_of_visite'    => $this->kind_of_visite,
            'day'               => $this->day->name,
               'day_id'               => $this->day->id,
            'max_visits_in_this_day'    => $this->day->number_of_visits,
        ];
    }
}
