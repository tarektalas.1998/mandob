<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function sadsad()
    {
        return $this->belongsTo(Appointment_time::class , 'appointment_time_id' , 'id')->with('day');
    }

    public function salesman()
    {
        return $this->belongsTo(Salesman::class);
    }
      public function doctor()
    {
        return $this->belongsTo(Doctor::class)->with('payment');
    }

}
