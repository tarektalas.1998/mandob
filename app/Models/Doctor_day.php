<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor_day extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    public function day()
    {
        return $this->hasOne(Day::class , 'id' , 'day_id');
    }
}
