<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Register_form extends Model
{
    use HasFactory;
    protected $guarded = [];
    
      public function password(): Attribute
    {
        return new Attribute(

            set: fn ($value) => Hash::make($value),
        );
    }
    
    
     public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function payment()
    {
        return $this->belongsTo(PaymentMethod::class , 'payment_method_id' , 'id');
    }
    
    //  public function password(): Attribute
    // {
    //     return new Attribute(

    //         set: fn ($value) => Hash::make($value),
    //     );
    // }
    
}
