<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Doctor extends Authenticatable
{
    use HasFactory , HasApiTokens;
    protected $guarded = [];
    protected $hidden = [
        // 'password',
        'created_at',
        'updated_at'
    ];

    public function password(): Attribute
    {
        return new Attribute(

            set: fn ($value) => Hash::make($value),
        );
    }

    public function times()
    {
        return $this->hasMany(Appointment_time::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function payment()
    {
        return $this->belongsTo(PaymentMethod::class , 'payment_method_id' , 'id');
    }
    
    public function interview()
    {
        return $this->hasMany(Interview::class);
    }
}
