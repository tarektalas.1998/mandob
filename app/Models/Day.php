<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    use HasFactory;
    protected $guarded = [];
    
     protected $hidden = [
        'created_at',
        'updated_at',
        'number_of_visits'
    ];
    
}
