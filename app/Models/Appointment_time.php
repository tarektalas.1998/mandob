<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Appointment_time extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at',

    ];

    public function times()
    {
        return $this->belongsTo(Doctor::class);
    }

     public function day()
     {
        return $this->belongsTo(Day::class);
     }

     public  function Time(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ,
            set :fn ($value) => date('H:i', strtotime($value)),
        );

    }
}
