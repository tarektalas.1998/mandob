<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Salesman extends Authenticatable
{
    use HasFactory , HasApiTokens;
    protected $table = 'salesmen';
    protected $guarded = [];

    protected $hidden = [
        'password',
        'created_at',
        'updated_at'
    ];

    public function password(): Attribute
    {
        return new Attribute(

            set: fn ($value) => Hash::make($value),
        );
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    
      public function interview()
    {
        return $this->belongsTo(Interview::class , 'id' , 'salesman_id')->with('sadsad')->where('doctor_id' , auth('doctor')->user()->id);
    }
    
    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }
    
}
