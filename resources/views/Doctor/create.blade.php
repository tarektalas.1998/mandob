@extends('layout')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Create New Doctor</h3>
                <form action="{{route('store_doctor')}}" method="post"  enctype="multipart/form-data">
                    @csrf

                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach

                    <div class="col-12 mx-auto">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">full name</label>
                            <input style="background-color : white; border-style: solid " type="text" name="full_name" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                        </div>
                        @error('full_name')
                        <p style="color:red">{{$message}}</p>
                        @enderror


                        <div class="col-12 mx-auto">
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">phone</label>
                                <input style="background-color : white; border-style: solid " type="text" name="phone" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                            </div>
                            @error('phone')
                            <p style="color:red">{{$message}}</p>
                            @enderror


                            <div class="col-12 mx-auto">
                                <div class="mb-3">
                                    <label for="exampleInputEmail1" class="form-label">specialization</label>
                                    <input style="background-color : white; border-style: solid " type="text" name="specialization" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                                </div>
                                @error('specialization')
                                <p style="color:red">{{$message}}</p>
                                @enderror


                                <div class="col-12 mx-auto">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">password</label>
                                        <input style="background-color : white; border-style: solid " type="password" name="password" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                                    </div>
                                    @error('password')
                                    <p style="color:red">{{$message}}</p>
                                    @enderror


                                    <div class="col-12 mx-auto">
                                        <div class="mb-3">
                                            <label for="exampleInputEmail1" class="form-label">cvv</label>
                                            <input style="background-color : white; border-style: solid " type="text" name="cvv" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                                        </div>
                                        @error('cvv')
                                        <p style="color:red">{{$message}}</p>
                                        @enderror
                                        
                                           <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">City</label>
                       
                            <select name="city_id">
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('city_id')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Payment</label>
                 
                            <select name="payment_method_id">
                                @foreach($pay as $p)
                                    <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('payment_method_id')
                        <p style="color:red">{{$message}}</p>
                        @enderror
                        
                         <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">certificate image</label>
                                 <input style="background-color : white; border-style: solid " type="file" name="image" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                           
                        </div>
                        @error('image')
                        <p style="color:red">{{$message}}</p>
                        @enderror


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

    </div>

</div>




@endsection
