@extends('layout')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Edit Doctors information</h3>
                <form action="{{route('doctor_update' , $doctor->id)}}" method="post"  >
                    @csrf

                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                    <div class="col-12 mx-auto">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Full name</label>
                            <input style="background-color : white; border-style: solid " type="text" name="full_name" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$doctor->full_name}}" required>
                        </div>
                        @error('full_name')
                        <p style="color:red">{{$message}}</p>
                        @enderror
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Phone number</label>
                            <input style="background-color : white; border-style: solid" type="text" name="phone" class="form-control p-2" id="exampleInputPassword1" value="{{$doctor->phone}}" required>
                        </div>
                        @error('phone')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Specialization</label>
                            <input style="background-color : white; border-style: solid" type="text" name="specialization" class="form-control p-2" id="exampleInputPassword1" value="{{$doctor->specialization}}" required>
                        </div>
                        @error('specialization')
                        <p style="color:red">{{$message}}</p>
                        @enderror
                        
                          <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Password (optional)</label>
                            <input style="background-color : white; border-style: solid" type="text" name="password" class="form-control p-2" id="exampleInputPassword1">
                        </div>
                        @error('password')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">EXP</label>
                            <input style="background-color : white; border-style: solid" type="text" name="exp" class="form-control p-2" id="exampleInputPassword1" value="{{$doctor->exp}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">CVV</label>
                            <input style="background-color : white; border-style: solid" type="text" name="cvv" class="form-control p-2" id="exampleInputPassword1" value="{{$doctor->cvv}}">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">City</label>
                            <input type="text" class="form-control p-2" id="exampleInputPassword1" value="{{$doctor->city->name}}" disabled>
                            <select name="city_id">
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('city_id')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Payment Method</label>
                            <input type="text" class="form-control p-2" id="exampleInputPassword1" value="{{$doctor->payment->name}}" disabled>
                            <select name="payment_method_id">
                                @foreach($payments as $payment)
                                    <option value="{{$payment->id}}">{{$payment->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('payment_method_id')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Certificate image</label>
                            <img src="{{asset('storage/'.$doctor->image)}}" width="250px" height="250px">
                        </div>
{{--                        <label for="exampleInputPassword1" class="form-label">Update image</label>--}}
{{--                        <input type="text" class="form-control p-2" id="exampleInputPassword1"  value="{{$doctor->image}}">--}}

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

    </div>

</div>




@endsection
