@extends('layout')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Edit Doctors information</h3>
                <form action="{{route('update_profile_admin' , $data->id)}}" method="post"  >
                    @csrf

                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach

                    <div class="col-12 mx-auto">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">admin name</label>
                            <input style="background-color : white; border-style: solid " type="text" name="full_name" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$data->full_name}}">
                        </div>
                        @error('full_name')
                        <p style="color:red">{{$message}}</p>
                        @enderror
                        
                         <div class="col-12 mx-auto">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">email</label>
                            <input style="background-color : white; border-style: solid " type="text" name="email" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$data->email}}">
                        </div>
                        @error('email')
                        <p style="color:red">{{$message}}</p>
                        @enderror
                        
                         <div class="col-12 mx-auto">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">password (اختياري)</label>
                            
                            <input style="background-color : white; border-style: solid " type="text" name="password" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                        </div>
                        @error('password')
                        <p style="color:red">{{$message}}</p>
                        @enderror


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

    </div>

</div>




@endsection
