@extends('layout')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Edit Doctors information</h3>
                <form action="{{route('store_city')}}" method="post"  >
                    @csrf

                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach

                    <div class="col-12 mx-auto">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">city name</label>
                            <input style="background-color : white; border-style: solid " type="text" name="name" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                        </div>
                        @error('name')
                        <p style="color:red">{{$message}}</p>
                        @enderror


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

    </div>

</div>




@endsection
