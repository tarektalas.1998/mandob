@extends('layout')
@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>


    @endif


<h3>تنبيه: عند حذف مدينة سيتم حذف كافة المستخدمين التابعين الى هذه المدينة</h3>

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Cities table</h6>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">City name</th>

                                    <th class="text-secondary opacity-7">edit</th>
                                    <th class="text-secondary opacity-7">delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)

                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1">
{{--                                            <div>--}}
{{--                                                <img src="{{asset('storage/'.$salesman->image)}}" class="avatar avatar-sm me-3 border-radius-lg" alt="user1">--}}
{{--                                            </div>--}}
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm">{{$d->name}}</h6>
{{--                                                <p class="text-xs text-secondary mb-0">john@creative-tim.com</p>--}}
                                            </div>
                                        </div>
                                    </td>

                                    <td class="align-middle">
                                       <a href="{{route('edit_city' , $d->id)}}" class=" font-weight-bold text-xs btn btn-primary " data-toggle="tooltip" data-original-title="Edit user">
                                            Edit
                                        </a>
                                    </td>
                                     <td class="align-middle">
                                       <a href="{{route('delete_city' , $d->id)}}" class=" font-weight-bold text-xs btn btn-primary " data-toggle="tooltip" data-original-title="Edit user">
                                            Delete
                                        </a>
                                    </td>
                                </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>




@endsection

{{--{{route('doctor_delete' , $doctor->id)}}--}}




<!-- Modal -->

