@extends('layout')
@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>


    @endif



    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Registered Salesman table</h6>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Salesman name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Phone</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">specialization</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">exp</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">cvv</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">city</th>
                                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">company</th>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Wallet Balance</th>
                                    <th class="text-secondary opacity-7"></th>
                                    <th class="text-secondary opacity-7"></th>
                                      <th class="text-secondary opacity-7"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($salesmen as $salesman)

                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1">
{{--                                            <div>--}}
{{--                                                <img src="{{asset('storage/'.$salesman->image)}}" class="avatar avatar-sm me-3 border-radius-lg" alt="user1">--}}
{{--                                            </div>--}}
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm">{{$salesman->name}}</h6>
{{--                                                <p class="text-xs text-secondary mb-0">john@creative-tim.com</p>--}}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{$salesman->phone}}</p>
{{--                                        <p class="text-xs text-secondary mb-0">Organization</p>--}}
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                        <span class="badge badge-sm bg-gradient-success">{{$salesman->specialization}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">{{$salesman->exp}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">{{$salesman->cvv}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">{{$salesman->city->name}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">{{$salesman->company}}</span>
                                    </td>
                                    
                                     <td class="align-middle text-center">
                                        <span class="text-secondary text-xs font-weight-bold">{{$salesman->wallet->balance ?? '0'}}</span>
                                    </td>
                                    
                                    <td class="align-middle">
                                       <a href="{{route('salesman_edit' , $salesman->id)}}" class=" font-weight-bold text-xs btn btn-primary " data-toggle="tooltip" data-original-title="Edit user">
                                            Edit
                                        </a>
                                    </td>
                                    <td class="align-middle">
                                       <a href="{{route('salesman_delete' , $salesman->id)}}" class=" font-weight-bold text-xs btn btn-primary " data-toggle="tooltip" data-original-title="Edit user">
                                            Delete
                                        </a>
                                    </td>
                                      <td class="align-middle">
                                        <a href="{{route('charge_page' , $salesman->id)}}" class=" font-weight-bold text-xs btn btn-primary " data-toggle="tooltip" data-original-title="Edit user">
                                            Add To Wallet
                                        </a>
                                    </td>
                                </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>




@endsection

{{--{{route('doctor_delete' , $doctor->id)}}--}}




<!-- Modal -->

