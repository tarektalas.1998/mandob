@extends('layout')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <h3>Edit Doctors information</h3>
                <form action="{{route('salesman_update' , $salesman->id)}}" method="post"  >
                    @csrf

                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach

                    <div class="col-12 mx-auto">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Full name</label>
                            <input style="background-color : white; border-style: solid " type="text" name="name" class="form-control p-2" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$salesman->name}}">
                        </div>
                        @error('name')
                        <p style="color:red">{{$message}}</p>
                        @enderror
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Phone number</label>
                            <input style="background-color : white; border-style: solid" type="text" name="phone" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->phone}}">
                        </div>
                        @error('phone')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Specialization</label>
                            <input style="background-color : white; border-style: solid" type="text" name="specialization" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->specialization}}">
                        </div>
                        @error('specialization')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">EXP</label>
                            <input style="background-color : white; border-style: solid" type="text" name="exp" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->exp}}">
                        </div>
                        @error('exp')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">office_name</label>
                            <input style="background-color : white; border-style: solid" type="text" name="office_name" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->office_name}}">
                        </div>
                        @error('office_name')
                        <p style="color:red">{{$message}}</p>
                        @enderror



                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">CVV</label>
                            <input style="background-color : white; border-style: solid" type="text" name="cvv" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->cvv}}">
                        </div>
                        @error('cvv')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">names</label>
                            <input style="background-color : white; border-style: solid" type="text" name="names" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->names}}">
                        </div>
                        @error('names')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">New Password</label>
                            <input style="background-color : white; border-style: solid" type="text" name="password" class="form-control p-2" id="exampleInputPassword1">
                        </div>
                        @error('password')
                        <p style="color:red">{{$message}}</p>
                        @enderror


                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">City</label>
                            <input type="text" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->city->name}}" disabled>
                            <select name="city_id">
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('city_id')
                        <p style="color:red">{{$message}}</p>
                        @enderror

                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Company</label>
                            <input name="company" style="background-color : white; border-style: solid" type="text" class="form-control p-2" id="exampleInputPassword1" value="{{$salesman->company}}">

                        </div>
                        @error('company_id')
                        <p style="color:red">{{$message}}</p>
                        @enderror



                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

    </div>

</div>




@endsection
